export function removeHtmlTags(strHtmlTags) {
  const cleanText = strHtmlTags.replace(/<\/?[^>]+(>|$)/g, '')

  return cleanText
}

export function articleDescription(strTextDescription = '') {
  const maxContentLength = 150
  strTextDescription = removeHtmlTags(strTextDescription)

  if (strTextDescription.length > maxContentLength) {
    strTextDescription = strTextDescription.substring(0, maxContentLength)

    return `${strTextDescription}...`
  }

  return strTextDescription
}
