FROM node:12.22.3-alpine

# create destination directory
RUN mkdir -p /usr/persis-fe
WORKDIR /usr/persis-fe

RUN echo "http://uk.alpinelinux.org/alpine/v3.11/main" > /etc/apk/repositories
RUN echo "https://uk.alpinelinux.org/alpine/v3.11/main" >> /etc/apk/repositories

# make sure you can use HTTPS
RUN apk --update add ca-certificates git

# copy the app, note .dockerignore
COPY . /usr/persis-fe
RUN npm install

# build necessary, even if no static files are needed,
# since it builds the server as well
RUN npm run build

# start the app
CMD [ "npm", "start" ]
