import colors from 'vuetify/es5/util/colors'
require('dotenv').config()

export default {
  googleAnalytics: {
    id: process.env.GOOGLE_ANALYTICS_VIEW,
  },

  publicRuntimeConfig: {
    googleAnalytics: {
      id: process.env.GOOGLE_ANALYTICS_VIEW,
    },
    axios: {
      baseURL: [
        process.env.BACKEND_BASE_URL,
        process.env.BACKEND_API_PREFIX,
      ].join('/'),
      proxy: false,
    },
  },

  privateRuntimeConfig: {
    googleAnalytics: {
      id: process.env.GOOGLE_ANALYTICS_VIEW,
    },
    axios: {
      baseURL: [
        process.env.BACKEND_BASE_URL,
        process.env.BACKEND_API_PREFIX,
      ].join('/'),
      proxy: false,
    },
  },

  badaso: {
    endpoint: process.env.BACKEND_BASE_URL,
    entities: { post: true, content: true },
    prefix: process.env.BACKEND_API_PREFIX,
  },

  axios: {
    baseURL: [
      process.env.BACKEND_BASE_URL,
      process.env.BACKEND_API_PREFIX,
    ].join('/'),
    proxy: false,
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - Website Official PP Persatuan Islam',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { hid: 'og:description', name: 'og:description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/webp', href: '/icon.webp' }],
    script: [
      {
        src: 'https://www.googletagmanager.com/gtag/js?id=G-8KB3K0K2MB',
        async: true,
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/assets/scss/persis.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['~/plugins/ufo.js'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/dotenv',
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/moment',
    '@nuxtjs/google-analytics',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    'nuxt-breakpoints',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    'nuxt-i18n',
    '@badaso/nuxt',
    '@nuxtjs/sentry',
  ],

  sentry: {
    dsn: process.env.SENTRY_DSN,
    // Additional Module Options go here
    // https://sentry.nuxtjs.org/sentry/options
    config: {
      environment: process.env.SENTRY_ENVIRONMENT,
      // Add native Sentry config here
      // https://docs.sentry.io/platforms/javascript/guides/vue/configuration/options/
    },
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/scss/_variables.scss'],
    theme: {
      dark: false,
      themes: {
        light: {
          primary: '#003D01',
          info: '#003D01',
        },
        dark: {
          primary: colors.shades.white,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: '#003D01',
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
    icons: {
      values: {
        sa: { component: () => import('~/components/icon/sa.vue') },
        id: { component: () => import('~/components/icon/id.vue') },
        us: { component: () => import('~/components/icon/us.vue') },
      },
    },
    defaultAssets: {
      icons: 'fa',
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  moment: {
    locales: ['id'],
  },

  breakpoints: {
    // default options
    sm: 480,
    md: 768,
    lg: 992,
    xl: 1024,
    options: {
      polyfill: true,
      throttle: 200,
    },
  },
}
