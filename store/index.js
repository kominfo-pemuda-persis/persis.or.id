
export const state = () => ({
  social: {
    facebook: null,
    twitter: null,
    instagram: null,
    youtube: null,
  },
})

export const mutations = {
  setSocial(state, social) {
    state.social.facebook = social.filter(val => val.name === 'facebook-link').pop().data.url
    state.social.twitter = social.filter(val => val.name === 'twitter-link').pop().data.url
    state.social.instagram = social.filter(val => val.name === 'instagram-link').pop().data.url
    state.social.youtube = social.filter(val => val.name === 'youtube-link').pop().data.url
  },
}