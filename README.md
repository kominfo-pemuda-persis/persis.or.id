# persis.or.id

## What you need to know before contributing

- This project using NUXT
- This project using BEM and SMACSS for CSS methodologies
- This project using "Mobile first" development concept
- This project using Yarn for js library management

## How to run development

### via local machine

```bash
# install dependencies
$ yarn install

# create environtment file & fill the required key
$ cp .env.example .env

# serve with hot reload at localhost:3000
$ yarn watch
```

### via docker and docker-compose

Make sure docker and docker-compose properly installed on your local machine

#### for Local Development

from root directory please execute command

```bash
docker-compose --project-directory . -f docker/docker-compose-local.yml build
```

```bash
docker-compose --project-directory . -f docker/docker-compose-local.yml up -d
```
