const ufo = require('ufo')

export default ({ app }, inject) => {
  inject('ufo', { ...ufo })
}
