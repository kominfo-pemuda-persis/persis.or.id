#!/bin/bash

echo \#\!\/bin\/bash >>init.sh

echo export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID >>init.sh
echo export AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION >>init.sh
echo export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY >>init.sh
echo export BACKEND_API_PREFIX=$BACKEND_API_PREFIX >>init.sh
echo export BACKEND_BASE_URL=$BACKEND_BASE_URL >>init.sh
echo export FACEBOOK_APP_ID=$FACEBOOK_APP_ID >>init.sh
echo export FRONTEND_BASE_URL=$FRONTEND_BASE_URL >>init.sh
echo export GOOGLE_ANALYTICS_VIEW=$GOOGLE_ANALYTICS_VIEW >>init.sh
echo export REPOSITORY_ECR_URL=$REPOSITORY_ECR_URL >>init.sh
echo export CONTAINER_APP_PROD=$CONTAINER_APP_PROD >>init.sh
echo export PROD_PORT_MAPPING=$PROD_PORT_MAPPING >>init.sh
echo export NUXT_PORT=$NUXT_PORT >>init.sh
echo export NUXT_HOST=$NUXT_HOST >>init.sh
echo export TAG_FE=$CI_COMMIT_REF_NAME >>init.sh

echo "aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin $ECR_URL" >>init.sh
echo docker compose down >>init.sh
echo docker system prune -af >>init.sh
echo docker pull $REPOSITORY_ECR_URL:$CI_COMMIT_REF_NAME >>init.sh
echo docker compose up -d >>init.sh
