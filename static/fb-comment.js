const facebook = {
  install: function(Vue, options) {
    (function (document, s, id) {
      var js, fbjs = document.getElementsByTagName(s)[0]
      if (document.getElementById(id)) return
      js = document.createElement(s)
      js.id = id
      js.src = "//connect.facebook.net/en_US/sdk.js"
      fbjs.parentNode?.insertBefore(js, fbjs)
      console.log('Setting up facebook SDK');
    }(document, 'script', 'facebook-jssdk'))

    window.fbAsyncInit = function onSDKInit() {
      FB.init(options)
      FB.AppEvents.logPageView()
      Vue.FB = FB
      window.dispatchEvent(new Event('fb-sdk-ready'))
    }

    Vue.FB = undefined
  }
}

import Vue from 'vue'
Vue.use(facebook, {
  appId: '494777751767500',
  autoLogAppEvents: true,
  xfbml: true,
  version: 'v11.0'
})